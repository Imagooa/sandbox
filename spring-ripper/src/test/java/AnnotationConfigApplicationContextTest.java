import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import quoters.Quoter;

@RunWith(JUnit4.class)
public class AnnotationConfigApplicationContextTest {

    @Test
    public void annotationConfigApplicationContextTest() {
        ApplicationContext ctx = new AnnotationConfigApplicationContext("quoters");
        Object quoter = ctx.getBean("quoter");
        Assert.assertTrue(ctx.getBean("firstBean") instanceof String);
        Assert.assertTrue(quoter instanceof Quoter);
        Assert.assertEquals("annotationCreatedStringBean", ((Quoter) quoter).getQuote());
        Assert.assertNotEquals(0, ((Quoter) quoter).getRepeat());
        ((Quoter) quoter).sayQuote();
    }
}
