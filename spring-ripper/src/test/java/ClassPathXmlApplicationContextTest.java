import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import quoters.Quoter;

@RunWith(JUnit4.class)
public class ClassPathXmlApplicationContextTest {

    @Test
    public void classPathXmlApplicationContextTest() {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("context.xml");
        Object quoter = ctx.getBean("quoter");
        Assert.assertTrue(ctx.getBean("firstBean") instanceof String);
        Assert.assertTrue(quoter instanceof Quoter);
        Assert.assertEquals("xmlCreatedStringBean", ((Quoter) quoter).getQuote());
        Assert.assertNotEquals(0, ((Quoter) quoter).getRepeat());
        ((Quoter) quoter).sayQuote();
    }
}
