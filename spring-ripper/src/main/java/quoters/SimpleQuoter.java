package quoters;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import quoters.annotation.AfterContextRefreshed;
import quoters.annotation.ProfiledMethod;
import quoters.annotation.WireRandomInt;

import javax.annotation.PostConstruct;

@Data
@Component("quoter")
public class SimpleQuoter implements Quoter, InitializingBean {

    private final String quote;

    @WireRandomInt(min = 4, max = 10)
    private int repeat;

    @Autowired
    public SimpleQuoter(String quote) {
        System.out.println("oneArgConstructor_SimpleQuoter");
        this.quote = quote;
    }

    @ProfiledMethod
    @Override
    public void sayQuote() {
        System.out.println("quote = " + quote + "__" + repeat);
    }

    @Override
    @AfterContextRefreshed
    public void saySomethingElse() {
        System.out.println("something_else");
    }

    public void init() {
        System.out.println("init_SimpleQuoter");
    }

    @Override
    public void afterPropertiesSet() {
        System.out.println("afterPropertiesSet_SimpleQuoter");
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("postConstruct_SimpleQuoter");
    }
}
