package quoters.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import quoters.annotation.AfterContextRefreshed;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Component
public class AfterContextRefreshedInvokerApplicationListener implements ApplicationListener<ContextRefreshedEvent>, BeanFactoryAware {

    private ConfigurableListableBeanFactory factory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        //Used only to support xml config
        this.factory = (ConfigurableListableBeanFactory) beanFactory;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        ApplicationContext applicationContext = event.getApplicationContext();
        for (String beanDefinitionName : factory.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = factory.getBeanDefinition(beanDefinitionName);
            String beanClassName = beanDefinition.getBeanClassName();
            try {
                Class<?> beanSourceClass = Class.forName(beanClassName);
                for (Method method : beanSourceClass.getMethods()) {
                    if (method.isAnnotationPresent(AfterContextRefreshed.class)) {
                        Object bean = applicationContext.getBean(beanDefinitionName);
                        Class<?> beanActualClass = bean.getClass();
                        Method actualMethod = beanActualClass.getMethod(method.getName(), method.getParameterTypes());
                        actualMethod.invoke(bean);
                    }
                }
            } catch (ClassNotFoundException e) {
                System.out.println("Cannot find class " + beanDefinitionName);
            } catch (NoSuchMethodException e) {
                System.out.println("Cannot find method in class " + beanDefinitionName);
            } catch (IllegalAccessException | InvocationTargetException e) {
                System.out.println("Cannot invoke method in class " + beanDefinitionName);
            } catch (NullPointerException e) {
                System.out.println("beanClassName is null for bean " + beanDefinitionName);
            }
        }
    }
}
