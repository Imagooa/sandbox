package quoters.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import quoters.annotation.ProfiledMethod;

import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ProfiledMethodAnnotationBeanPostProcessor implements BeanPostProcessor {

    private final AppConfig appConfig = new AppConfig();
    private Map<String, AnnotatedClassInfo> profiledClasses = new HashMap<>();

    public ProfiledMethodAnnotationBeanPostProcessor() {

        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        try {
            platformMBeanServer.registerMBean(appConfig, new ObjectName("profiling", "name", "appConfig"));
        } catch (JMException e) {
            System.out.println("Connot register profiling MBean");
        }
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> beanClass = bean.getClass();
        Set<String> annotatedMethods = Arrays.stream(beanClass.getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(ProfiledMethod.class))
                .map(Method::getName)
                .collect(Collectors.toSet());
        if (!annotatedMethods.isEmpty()) {
            profiledClasses.put(beanName, new AnnotatedClassInfo(beanClass, annotatedMethods));
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        AnnotatedClassInfo annotatedClassInfo = profiledClasses.get(beanName);
        if (annotatedClassInfo != null) {
            Class beanClass = annotatedClassInfo.getClazz();
            return Proxy.newProxyInstance(beanClass.getClassLoader(), beanClass.getInterfaces(), (proxy, method, args) -> {
                Object returnValue;
                boolean methodIsAnnotated = annotatedClassInfo.getAnnotatedMethodNames().contains(method.getName());
                boolean profilingIsEnabled = appConfig.isProfilingEnabled();
                if (methodIsAnnotated && profilingIsEnabled) {
                    System.out.println(String.format("proxy_wrapper_on_bean_%s_method_%s_start", beanName, method.getName()));
                    long startTime = System.currentTimeMillis();
                    returnValue = method.invoke(bean, args);
                    long finishTime = System.currentTimeMillis();
                    long length = finishTime - startTime;
                    System.out.println(String.format("proxy_wrapper_on_bean_%s_method_%s_finish_Total_%dms", beanName, method.getName(), length));
                } else {
                    returnValue = method.invoke(bean, args);
                }

                return returnValue;
            });
        }
        return bean;
    }

    @Data
    @AllArgsConstructor
    private static class AnnotatedClassInfo {
        private Class clazz;

        Set<String> annotatedMethodNames;
    }
}
