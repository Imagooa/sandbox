package quoters.config;

public class AppConfig implements AppConfigMBean {

    private boolean isProfilingEnabled;

    @Override
    public boolean isProfilingEnabled() {
        return isProfilingEnabled;
    }

    @Override
    public void setProfilingEnabled(boolean profilingEnabled) {
        isProfilingEnabled = profilingEnabled;
    }
}
