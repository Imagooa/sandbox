package quoters.config;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import quoters.annotation.WireRandomInt;
import quoters.util.IntRandomizer;

import java.lang.reflect.Field;

@Component
public class WireRandomIntAnnotationBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessBeforeInitialization_" + bean.getClass().getSimpleName());
        Field[] declaredFields = bean.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(WireRandomInt.class)) {
                WireRandomInt annotation = field.getAnnotation(WireRandomInt.class);
                int randomvalue = IntRandomizer.getRandomIntInInterval(annotation.min(), annotation.max());
                field.setAccessible(true);
                ReflectionUtils.setField(field, bean, randomvalue);
            }
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        System.out.println("postProcessAfterInitialization_" + bean.getClass().getSimpleName());
        return bean;
    }
}
