package quoters.config;

public interface AppConfigMBean {

    boolean isProfilingEnabled();

    void setProfilingEnabled(boolean profilingEnabled);
}
