package quoters.util;

import java.util.Random;

public class IntRandomizer {
    public static int getRandomIntInInterval(int min, int max) {
        Random random = new Random();
        return min + random.nextInt(max - min);
    }
}
