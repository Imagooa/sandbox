package quoters;

public interface Quoter {
    void sayQuote();

    int getRepeat();

    String getQuote();

    void saySomethingElse();

}
