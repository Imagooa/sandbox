package quoters;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfiguration {

    @Bean
    public String firstBean() {
        return "annotationCreatedStringBean";
    }
}
