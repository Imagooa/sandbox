package kanatnikov.oleg.sandbox.algorithms;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class ArraySortingUtilTest {

    private static final int LIST_SIZE = 10;
    private static final int NUMBER_ORIGIN = 0;
    private static final int NUMBER_BOUND = 99;
    private List<Integer> unsorted;
    private List<Integer> check;

    @Before
    public void setUp() {
        unsorted = new Random()
                .ints(LIST_SIZE, NUMBER_ORIGIN, NUMBER_BOUND)
                .boxed()
                .collect(Collectors.toList());
        List<Integer> copy = new ArrayList<>(unsorted);
        Collections.sort(copy);
        this.check = copy;
    }

    @Test
    public void bubbleSort() {
        List<Integer> sorted = ArraySortingUtil.bubbleSort(unsorted);
        assertEquals(check, sorted);
    }

    @Test
    public void insertionSort() {
        List<Integer> sorted = ArraySortingUtil.insertionSort(unsorted);
        assertEquals(check, sorted);
    }

    @Test
    public void quickSort() {
        System.out.println(unsorted);
        List<Integer> sorted = ArraySortingUtil.quickSort(unsorted);
        assertEquals(check, sorted);
    }
}