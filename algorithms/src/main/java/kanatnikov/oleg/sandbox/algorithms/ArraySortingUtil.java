package kanatnikov.oleg.sandbox.algorithms;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ArraySortingUtil {

    public static List<Integer> insertionSort(List<Integer> list) {
        for (int comparedIndex = 1; comparedIndex < list.size(); comparedIndex++) {
            int comparedElem = list.get(comparedIndex);
            int currentIndex = comparedIndex - 1;
            while (currentIndex >= 0 && list.get(currentIndex) > comparedElem) {
                list.set(currentIndex + 1, list.get(currentIndex));
                currentIndex--;
            }
            list.set(currentIndex + 1, comparedElem);
        }
        return list;
    }

    public static List<Integer> bubbleSort(List<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            boolean isModified = false;
            for (int j = 1; j < list.size(); j++) {
                if (list.get(j - 1) > list.get(j)) {
                    Collections.swap(list, j - 1, j);
                    isModified = true;
                }
            }
            if (!isModified) {
                return list;
            }
        }
        return list;
    }

    public static List<Integer> quickSort(List<Integer> list) {
        int length = list.size();
        if (length == 1) {
            return list;
        }
        quickSort(list, 0, length - 1);
        return list;
    }

    private static void quickSort(List<Integer> list, int lowerIndex, int higherIndex) {
        if (lowerIndex < higherIndex) {
            int partitionIndex = partition(list, lowerIndex, higherIndex);

            quickSort(list, lowerIndex, partitionIndex - 1);
            quickSort(list, partitionIndex + 1, higherIndex);
        }
    }

    private static int partition(List<Integer> list, int lowerIndex, int higherIndex) {
        int pivot = list.get(higherIndex);
        int partitionIndex = lowerIndex - 1;
        for (int currentIndex = lowerIndex; currentIndex < higherIndex; currentIndex++) {
            if (list.get(currentIndex) < pivot) {
                partitionIndex++;
                Collections.swap(list, partitionIndex, currentIndex);
            }
        }
        Collections.swap(list, partitionIndex + 1, higherIndex);
        return partitionIndex + 1;
    }
}
